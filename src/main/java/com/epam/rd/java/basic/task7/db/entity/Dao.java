package com.epam.rd.java.basic.task7.db.entity;

import com.epam.rd.java.basic.task7.db.DBException;

import java.util.List;

public interface Dao {
    List<User> findAllUsers() throws DBException;
    boolean setTeamsForUser(User user, Team... teams) throws DBException;
    boolean updateTeam(Team team) throws DBException;
    boolean deleteTeam(Team team) throws DBException;
    List<Team> getUserTeams(User user) throws DBException;
    boolean insertTeam(Team team) throws DBException;
    List<Team> findAllTeams() throws DBException;
    Team getTeam(String name) throws DBException;
    User getUser(String login) throws DBException;
    boolean deleteUsers(User... users) throws DBException;
    boolean insertUser(User user) throws DBException;
}
