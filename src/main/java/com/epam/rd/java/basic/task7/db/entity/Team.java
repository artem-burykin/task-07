package com.epam.rd.java.basic.task7.db.entity;

import java.sql.Statement;
import java.util.Objects;

public class Team {

	private int id;

	private String name;

	public Team(){
	}
	private Team(int id, String name){
		this.id = id;
		this.name = name;
	}
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public static Team createTeam(String name) {
		return new Team(0, name);
	}

	public String toString() {
		return name;
	}

	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		}

		if (obj == null || obj.getClass() != this.getClass()) {
			return false;
		}

		Integer id1 = id;
		Team team = (Team) obj;
		return id == team.id
				&& (name == team.name
				|| (name != null && name.equals(team.getName()))) && (id1 == team.id
				|| (id1 != null && id1.equals(team.getId())
		));
	}

	public int hashCode() {
		return 0;
	}
}
