package com.epam.rd.java.basic.task7.db;

public abstract class DBConstant {
    private DBConstant(){}

    //Fields
    public static final String SELECT_FROM_USERS_ORDER_BY_LOGIN = "SELECT * FROM users;";
    public static final String INSERT_USER = "INSERT INTO users (login) VALUES (?);";
    public static final String SELECT_FROM_TEAMS_ORDER_BY_NAME = "SELECT * FROM teams;";
    public static final String INSERT_TEAM = "INSERT INTO teams (name) VALUES (?);";
    public static final String SET_TEAM_USER = "INSERT INTO users_teams (user_id, team_id) VALUES (?, ?)";
    public static final String GET_USER_TEAMS = "SELECT teams.id, teams.name FROM users join users_teams on users.id = users_teams.user_id join teams on users_teams.team_id=teams.id WHERE users_teams.user_id = (SELECT users.id FROM users where users.login = ?);";
    public static final String GET_USER = "SELECT * FROM users WHERE users.login = ?;";
    public static final String GET_TEAM = "SELECT * FROM teams WHERE teams.name = ?;";
    public static final String DELETE_USER = "DELETE FROM users WHERE users.login = ?;";
    public static final String DELETE_TEAM = "DELETE FROM teams WHERE teams.name = ?;";
    public static final String UPDATE_TEAM = "UPDATE teams SET teams.name = ? WHERE teams.id = ?;";

    //Queries
    public static final String F_USER_ID = "id";
    public static final String F_USER_LOGIN = "login";
    public static final String F_TEAM_ID = "id";
    public static final String F_TEAM_NAME = "name";
}
