package com.epam.rd.java.basic.task7.db.entity;

import java.util.Objects;

public class User {

	private int id;

	private String login;

	public User() {
	}

	private User(int id, String login) {
		this.id = id;
		this.login = login;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public static User createUser(String login) {
		return new User(0, login);
	}

	@Override
	public String toString() {
		return login;
	}

	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		}

		if (obj == null || obj.getClass() != this.getClass()) {
			return false;
		}

		Integer id1 = id;
		User user = (User) obj;
		return id == user.id
				&& (login == user.login
				|| (login != null &&login.equals(user.getLogin())))        && (id1 == user.id
				|| (id1 != null && id1.equals(user.getId())
		));
	}

	@Override
	public int hashCode() {
		return 0;
	}
}
