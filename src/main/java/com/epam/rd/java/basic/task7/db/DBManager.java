package com.epam.rd.java.basic.task7.db;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager implements Dao {
//	private static final String URL = "jdbc:mysql://localhost:3306/test2db";
//	private static final String USER = "root";
//	private static final String PASSWORD = "root";
//	private static DBManager instance;
//
//	public static synchronized DBManager getInstance() {
//		if (instance == null) {
//			instance = new DBManager();
//		}
//		return instance;
//	}
//
//	private DBManager() {
//	}
//
//	public void close (AutoCloseable con){
//		if (con != null){
//			try {
//				con.close();
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
//		}
//	}
//
//	public Connection getConnection(boolean autocommit) throws SQLException{
//		Connection con;
//		con = DriverManager.getConnection(URL, USER, PASSWORD);
//		con.setAutoCommit(autocommit);
//		return con;
//	}
//
//	static void rollback(Connection con) throws SQLException {
//		if (con != null) {
//			con.rollback();
//		}
//	}
//
//	@Override
//	public List<User> findAllUsers() throws DBException {
//		List<User> result = new ArrayList<>();
//		try(Connection con = getConnection(false); Statement stmt = con.createStatement(); ResultSet rs = stmt.executeQuery(DBConstant.SELECT_FROM_USERS_ORDER_BY_LOGIN)){
//			while(rs.next()){
//				User user = User.createUser(rs.getString(DBConstant.F_USER_LOGIN));
//				user.setId(rs.getInt(DBConstant.F_USER_ID));
//				result.add(user);
//			}
//		}
//		catch (SQLException e){
//			e.printStackTrace();
//			throw new DBException("No user in the database", e);
//		}
//		return result;
//	}
//
//	@Override
//	public void insertUser(User user) throws DBException {
//		Connection con = null;
//		try {
//			con = getConnection(false);
//			insertUser(con, user);
//			con.commit();
//		} catch (SQLException e) {
//			// log
//			e.printStackTrace();
//			// rethrow the exception
//			throw new DBException("Cannot add user", e);
//		} finally {
//			close(con);
//		}
//	}
//
//	private void insertUser(Connection con, User user) throws SQLException {
//		try (PreparedStatement st = con.prepareStatement(DBConstant.INSERT_USER, Statement.RETURN_GENERATED_KEYS);) {
//			int i = 0;
//			st.setString(++i, user.getLogin());
//			int c = st.executeUpdate();
//			if (c > 0) {
//				try (ResultSet keys = st.getGeneratedKeys();) {
//					if (keys.next()) {
//						user.setId(keys.getInt(1));
//					}
//				}
//			}
//		}
//	}
//
//	@Override
//	public boolean deleteUsers(User... users) throws DBException {
//		Connection con = null;
//		PreparedStatement stmt = null;
//		try {
//			con = getConnection(false);
//			con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
//			stmt = con.prepareStatement(DBConstant.DELETE_USER, Statement.RETURN_GENERATED_KEYS);
//			for (User user : users) {
//				stmt.setString(1, user.getLogin());
//				stmt.execute();
//			}
//			con.commit();
//			return true;
//		} catch (SQLException e) {
//			try {
//				rollback(con);
//			} catch (SQLException ex) {
//				ex.printStackTrace();
//				throw new DBException("Cannot set user teams", e);
//			}
//			// log
//			e.printStackTrace();
//			// rethrow the exception
//			throw new DBException("Cannot set user teams", e);
//		} finally {
//			close(con);
//			close(stmt);
//		}
//	}
//
//	@Override
//	public User getUser(String login) throws DBException {
//		Connection con = null;
//		PreparedStatement stmt = null;
//		ResultSet rs = null;
//		User user;
//		try{
//			con = getConnection(false);
//			stmt = con.prepareStatement(DBConstant.GET_USER);
//			stmt.setString(1, login);
//			rs = stmt.executeQuery();
//			rs.next();
//			user = User.createUser(rs.getString(2));
//			user.setId(rs.getInt(1));
//		}
//		catch (SQLException e){
//			e.printStackTrace();
//			throw new DBException("This users not found", e);
//		}
//		finally {
//			close(con);
//			close(stmt);
//			close(rs);
//		}
//		return user;
//	}
//
//	@Override
//	public Team getTeam(String name) throws DBException {
//		Connection con = null;
//		PreparedStatement stmt = null;
//		ResultSet rs = null;
//		Team team;
//		try{
//			con = getConnection(false);
//			stmt = con.prepareStatement(DBConstant.GET_TEAM);
//			stmt.setString(1, name);
//			rs = stmt.executeQuery();
//			rs.next();
//			team = Team.createTeam(rs.getString(2));
//			team.setId(rs.getInt(1));
//		}
//		catch (SQLException e){
//			e.printStackTrace();
//			throw new DBException("This users not found", e);
//		}
//		finally {
//			close(con);
//			close(stmt);
//			close(rs);
//		}
//		return team;
//	}
//
//	@Override
//	public List<Team> findAllTeams() throws DBException {
//		List<Team> result = new ArrayList<>();
//		try(Connection con = getConnection(false); Statement stmt = con.createStatement(); ResultSet rs = stmt.executeQuery(DBConstant.SELECT_FROM_TEAMS_ORDER_BY_NAME)){
//			while(rs.next()){
//				Team team = Team.createTeam(rs.getString(DBConstant.F_TEAM_NAME));
//				team.setId(rs.getInt(DBConstant.F_TEAM_ID));
//				result.add(team);
//			}
//		}
//		catch (SQLException e){
//			e.printStackTrace();
//			throw new DBException("No user in the database", e);
//		}
//		return result;
//	}
//
//	@Override
//	public void insertTeam(Team team) throws DBException {
//		Connection con = null;
//		try {
//			con = getConnection(false);
//			insertTeam(con, team);
//			con.commit();
//		} catch (SQLException e) {
//			// log
//			e.printStackTrace();
//			// rethrow the exception
//			throw new DBException("Cannot add team", e);
//		} finally {
//			close(con);
//		}
//	}
//
//	private void insertTeam(Connection con, Team team) throws SQLException {
//		try (PreparedStatement st = con.prepareStatement(DBConstant.INSERT_TEAM, Statement.RETURN_GENERATED_KEYS);) {
//			int i = 0;
//			st.setString(++i, team.getName());
//			int c = st.executeUpdate();
//			if (c > 0) {
//				try (ResultSet keys = st.getGeneratedKeys();) {
//					if (keys.next()) {
//						team.setId(keys.getInt(1));
//					}
//				}
//			}
//		}
//	}
//
//	@Override
//	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
//		Connection con = null;
//		PreparedStatement stmt = null;
//		try {
//			con = getConnection(false);
//			con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
//			stmt = con.prepareStatement(DBConstant.SET_TEAM_USER, Statement.RETURN_GENERATED_KEYS);
//			stmt.setInt(1, user.getId());
//			for (Team team : teams) {
//				stmt.setInt(2, team.getId());
//				stmt.execute();
//			}
//			con.commit();
//			return true;
//		} catch (SQLException e) {
//			try {
//				rollback(con);
//			} catch (SQLException ex) {
//				ex.printStackTrace();
//				throw new DBException("Cannot delete one of those users", e);
//			}
//			// log
//			e.printStackTrace();
//			// rethrow the exception
//			throw new DBException("Cannot delete one of those users", e);
//		} finally {
//			close(con);
//			close(stmt);
//		}
//	}
//
//	@Override
//	public List<Team> getUserTeams(User user) throws DBException {
//		List<Team> result = new ArrayList<>();
//		Connection con = null;
//		PreparedStatement stmt = null;
//		ResultSet rs = null;
//
//		try {
//			con = getConnection(false);
//			stmt = con.prepareStatement(DBConstant.GET_USER_TEAMS);
//			stmt.setString(1, user.getLogin());
//			rs = stmt.executeQuery();
//			while(rs.next()){
//				Team team = Team.createTeam(rs.getString(2));
//				team.setId(rs.getInt(1));
//				result.add(team);
//			}
//		}
//		catch (SQLException e){
//			e.printStackTrace();
//			throw new DBException("No user in the database", e);
//		}
//		finally {
//			close(con);
//			close(stmt);
//			close(rs);
//		}
//		return result;
//	}
//
//	@Override
//	public boolean deleteTeam(Team team) throws DBException {
//		Connection con = null;
//		PreparedStatement stmt = null;
//		try {
//			con = getConnection(false);
//			con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
//			stmt = con.prepareStatement(DBConstant.DELETE_TEAM, Statement.RETURN_GENERATED_KEYS);
//			stmt.setString(1, team.getName());
//			stmt.execute();
//			con.commit();
//			return true;
//		} catch (SQLException e) {
//			try {
//				rollback(con);
//			} catch (SQLException ex) {
//				ex.printStackTrace();
//				throw new DBException("Cannot delete team", e);
//			}
//			// log
//			e.printStackTrace();
//			// rethrow the exception
//			throw new DBException("Cannot delete team", e);
//		} finally {
//			close(con);
//			close(stmt);
//		}
//	}
//
//	@Override
//	public boolean updateTeam(Team team) throws DBException {
//		Connection con = null;
//		PreparedStatement stmt = null;
//		try {
//			con = getConnection(false);
//			con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
//			stmt = con.prepareStatement(DBConstant.UPDATE_TEAM, Statement.RETURN_GENERATED_KEYS);
//			stmt.setString(1, team.getName());
//			stmt.setInt(2,team.getId());
//			stmt.execute();
//			con.commit();
//			return true;
//		} catch (SQLException e) {
//			try {
//				rollback(con);
//			} catch (SQLException ex) {
//				ex.printStackTrace();
//				throw new DBException("Cannot update team", e);
//			}
//			// log
//			e.printStackTrace();
//			// rethrow the exception
//			throw new DBException("Cannot update team", e);
//		} finally {
//			close(con);
//			close(stmt);
//		}
//	}
	private static DBManager instance;

	public static synchronized DBManager getInstance() {
		if (instance == null) {
			instance = new DBManager();
		}
		return instance;
	}

	private DBManager() {
		try {
			Class.forName("org.postgresql.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	private static Connection getConnection() {
		Properties properties = new Properties();
		try {
			InputStream in = new FileInputStream("app.properties");
			properties.load(in);
			in.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		Connection connection = null;

		String connectionURL = properties.getProperty("connection.url");
		String username = properties.getProperty("username");
		String password = properties.getProperty("password");

		try {
			connection = DriverManager.getConnection(connectionURL, username, password);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return connection;
	}

	public List<User> findAllUsers() throws DBException {
		List<User> users = new ArrayList<>();

		try (Connection connection = getConnection()) {
			Statement findAllStatement = connection.createStatement();

			ResultSet resultSet = findAllStatement.executeQuery(
					"SELECT * FROM users");

			while (resultSet.next()) {
				User user = new User();
				user.setId(resultSet.getInt("id"));
				user.setLogin(resultSet.getString("login"));

				users.add(user);
			}
		} catch (SQLException e) {

		}
		return users;
	}

	public boolean insertUser(User user) throws DBException {
		if (user == null) return false;
		try (Connection connection = getConnection()) {
			PreparedStatement insertStatement = connection.prepareStatement(
					"INSERT INTO users(login) VALUES(?)", Statement.RETURN_GENERATED_KEYS);

			insertStatement.setString(1, user.getLogin());

			int affectedRows = insertStatement.executeUpdate();
			if (affectedRows == 0) return false;

			ResultSet resultSet = insertStatement.getGeneratedKeys();
			if (resultSet.next()) user.setId(resultSet.getInt(1));
			return true;
		} catch (SQLException e) {
			throw new DBException("Cannot insert user" + user.getLogin(), e.getCause());
		}
	}

	public boolean deleteUsers(User... users) throws DBException {
		try (Connection connection = getConnection()) {
			PreparedStatement deleteUserStatement = connection.prepareStatement(
					"DELETE FROM users WHERE login=?");

			for (User user : users) {
				deleteUserStatement.setString(1, user.getLogin());
				deleteUserStatement.executeUpdate();
			}

		} catch (SQLException e) {
			throw new DBException("Cannot delete users", e.getCause());
		}
		return true;
	}

	public User getUser(String login) throws DBException {
		User user = new User();
		try (Connection connection = getConnection()) {
			PreparedStatement getUserStatement = connection.prepareStatement(
					"SELECT * FROM users WHERE login=?");

			getUserStatement.setString(1, login);

			ResultSet resultSet = getUserStatement.executeQuery();

			resultSet.next();

			user.setId(resultSet.getInt("id"));
			user.setLogin(login);

		} catch (SQLException e) {
			throw new DBException("Cannot get user " + login, e.getCause());
		}
		return user;
	}

	public Team getTeam(String name) throws DBException {
		Team team = new Team();
		try (Connection connection = getConnection()) {
			PreparedStatement getTeamStatement = connection.prepareStatement(
					"SELECT * FROM teams WHERE name=?");

			getTeamStatement.setString(1, name);

			ResultSet resultSet = getTeamStatement.executeQuery();

			resultSet.next();

			team.setId(resultSet.getInt("id"));
			team.setName(name);

		} catch (SQLException e) {
			throw new DBException("Cannot get team " + name, e.getCause());
		}
		return team;
	}

	public List<Team> findAllTeams() throws DBException {
		List<Team> teams = new ArrayList<>();
		try (Connection connection = getConnection()) {
			Statement findAllTeamsStatement = connection.createStatement();
			ResultSet resultSet = findAllTeamsStatement.executeQuery(
					"SELECT * FROM teams");

			while (resultSet.next()) {
				Team team = new Team();
				team.setId(resultSet.getInt("id"));
				team.setName(resultSet.getString("name"));
				teams.add(team);
			}
		} catch (SQLException e) {
			throw new DBException("Cannot find all teams", e.getCause());
		}
		return teams;
	}

	public boolean insertTeam(Team team) throws DBException {
		try (Connection connection = getConnection()) {
			PreparedStatement insertStatement = connection.prepareStatement(
					"INSERT INTO teams(name) VALUES(?)", Statement.RETURN_GENERATED_KEYS);

			insertStatement.setString(1, team.getName());

			int affectedRows = insertStatement.executeUpdate();
			if (affectedRows == 0) return false;
			ResultSet resultSet = insertStatement.getGeneratedKeys();
			if(resultSet.next()) team.setId(resultSet.getInt(1));

		} catch (SQLException e) {

		}
		return true;
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		Connection connection = null;
		try {
			connection = getConnection();
			connection.setAutoCommit(false);

			for (Team team : teams) {
				PreparedStatement insertStatement = connection.prepareStatement(
						"INSERT INTO users_teams(user_id, team_id) VALUES(?, ?)");

				insertStatement.setInt(1, user.getId());
				insertStatement.setInt(2, team.getId());

				insertStatement.executeUpdate();
			}

			connection.commit();
			connection.close();
			return true;

		} catch (SQLException e) {
			try {
				connection.rollback();

			} catch (SQLException ex) {

			}
			throw new DBException("Cannot set Teams for User", e.getCause());
		} finally {
			try {
				if (connection != null) {
					connection.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public List<Team> getUserTeams(User user) throws DBException {
		List<Team> teams = new ArrayList<>();
		List<Integer> teamsId = new ArrayList<>();

		try (Connection connection = getConnection()) {
			PreparedStatement findAllTeams = connection.prepareStatement(
					"SELECT * FROM users_teams WHERE user_id=?");
			findAllTeams.setInt(1, user.getId());

			ResultSet resultSet = findAllTeams.executeQuery();

			while (resultSet.next()) {
				teamsId.add(resultSet.getInt("team_id"));
			}
		} catch (SQLException e) {
			throw new DBException("Cannot get user teams", e.getCause());
		}

		for (Integer id : teamsId) {
			try (Connection connection = getConnection()) {
				PreparedStatement findAllTeams = connection.prepareStatement(
						"SELECT * FROM teams WHERE id=?");
				findAllTeams.setInt(1, id);

				ResultSet resultSet = findAllTeams.executeQuery();
				while (resultSet.next()) {
					Team team = new Team();
					team.setId(id);
					team.setName(resultSet.getString("name"));
					teams.add(team);
				}
			} catch (SQLException e) {
				throw new DBException("Cannot get user teams", e.getCause());
			}
		}
		return teams;
	}

	public boolean deleteTeam(Team team) throws DBException {
		try (Connection connection = getConnection()) {
			PreparedStatement deleteTeamStatement = connection.prepareStatement(
					"DELETE FROM teams WHERE id =? AND name=?");

			deleteTeamStatement.setInt(1, team.getId());
			deleteTeamStatement.setString(2, team.getName());

			deleteTeamStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DBException("Cannot delete team", e.getCause());
		}
		return true;
	}

	public boolean updateTeam(Team team) throws DBException {
		try (Connection connection = getConnection()) {
			PreparedStatement updateTeamStatement = connection.prepareStatement(
					"UPDATE teams SET name =? WHERE id=?");

			updateTeamStatement.setString(1, team.getName());
			updateTeamStatement.setInt(2, team.getId());

			updateTeamStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DBException("Cannot update team", e.getCause());
		}
		return true;
	}
}
